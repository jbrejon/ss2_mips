### Display Stages ###
  I  |  D  | E0  | E1  | M0  | M1  | W0  | W1  
none |none |none |none |none |none |none |none 
none |none |addi |lui  |none |none |none |none 
none |none |add  |ori  |addi |lui  |none |none 
none |none |lw   |nop* |add  |ori  |addi |lui  
none |none |lbu  |nop* |lw   |nop* |add  |ori  
none |none |lbu  |nop* |none |none |lw   |nop* 
none |none |ori  |nop* |lbu  |nop* |none |none 
none |none |ori  |nop* |none |none |lbu  |nop* 
none |none |none |none |ori  |nop* |none |none 
none |none |bltzal|addu |none |none |ori  |nop* 
none |none |add  |addiu|bltzal|addu |none |none 
none |none |none |none |add  |addiu|bltzal|addu 
none |none |none |none |none |none |add  |addiu
none |none |none |none |none |none |none |none 
### Emul ###
instructions : 14 - cycles : 14
  	  1  2  3  4  5  6  7  8  9 10 11 12 13 14
addi	 mI mD E0 M0 W0                           
lui	       E1 M1 W1                           
add	    mI mD E0 M0 W0                        
ori	          E1 M1 W1                        
lw	       mI mD E0 M0 W0                     
nop*	             E1 M1 W1                     
lbu	          mI mD FZ E0 M0 W0               
nop*	                FZ E1 M1 W1               
ori	             mI mC mD FZ E0 M0 W0         
nop*	                      FZ E1 M1 W1         
bltzal	                      mC mC mD E0 M0 W0   
addu	                               E1 M1 W1   
nop	                            mI mD E0 M0 W0
addiu	                                  E1 M1 W1
### Bypasses ###
cycle :  3 | inst1 :  3 | inst2 :  1 | op : 0
cycle :  3 | inst1 :  2 | inst2 :  0 | op : 0
cycle :  3 | inst1 :  2 | inst2 :  0 | op : 1
cycle :  4 | inst1 :  4 | inst2 :  3 | op : 0
cycle :  6 | inst1 :  6 | inst2 :  4 | op : 0
cycle :  8 | inst1 :  8 | inst2 :  6 | op : 0
cycle :  9 | inst1 : 10 | inst2 :  8 | op : 0
cycle : 11 | inst1 : 13 | inst2 : 10 | op : 0
