#include <iostream>

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "Program.h"
#include "Mips.h"
#include "Svgss2.h"
#include "EmulProg.h"


int error(std::string mes)
{
    std::cout << mes << std::endl;
    exit(-1);
}

int main(int argc, char * argv[])
{
    std::string arg;
    std::string file = std::string("");
    int check[4] = {0};
    int alignement = 1;
    int verbose_level = 0;
    int * branch_tab = new int[30];
    std::string output = std::string("output.svg");
    for (int i = 0; i < 30; i++)
        branch_tab[i] = 0;
    if (argc < 3)
    {
        std::cout << "Usage : " << argv[0] << " [options] -f <file>, --file <file>" << std::endl;
        std::cout << "Options :" << std::endl;
        std::cout << "-bt <value>, --branch-taken <value>  valid values : [0, 29] Default : no conditional branch is taken" << std::endl;
        std::cout << "-fb <value>, --final-branch <value>  valid values : [0, 29] Default : no branch is final" << std::endl;
        std::cout << "                                     The branch is taken and the program is stopped after" << std::endl;
        std::cout << "                                     the execution of the instruction following the" << std::endl;
        std::cout << "                                     delayed slot." << std::endl;
        std::cout << "-a <value>, --alignement <value>     valid values : [0, 1]  Default : first address is aligned (alignement = 1)" << std::endl;
        std::cout << "-o <file>, --output <file>                                  Default : ./output.svg" << std::endl;
        std::cout << "-v <value>, --verbose <value>        valid values : [0, 5]  Default : 0 (no trace)" << std::endl;
        return 0;
    }
    else
    {
        for (int i = 1; i < argc - 1; i++)
        {
            if (i + 1 > argc - 1)
                error("Wrong arguments");
            arg = std::string(argv[i]);
            if (arg == "--file" || arg == "-f")
            {
                if (!check[0])
                {
                    file = std::string(argv[i+1]);
                    check[0] = 1;
                }
                else
                {
                    error("Error : file set multiple times");
                }
            }
            else if (arg == "-bt" || arg == "--branch-taken")
            {
                if (atoi(argv[i + 1]) >= 30 || atoi(argv[i + 1]) < 0)
                {
                    error("Error : Branch must be [0, 30[");
                }
                branch_tab[atoi(argv[i + 1])] = 1;
            }
            else if (arg == "-o" || arg == "--output")
            {
                if (!check[1])
                {
                    check[1] = 1;
                    output = argv[i + 1];
                }
                else
                {
                    error("Error : output set multiple times");
                }
            }
            else if (arg == "-a" || arg == "--alignement")
            {
                if (!check[2])
                {
                    if ((atoi(argv[i + 1]) != 0) && (atoi(argv[i + 1]) != 1))
                    {
                        error("Error : alignement can only be 0 or 1");
                    }
                    check[2] = 1;
                    alignement = atoi(argv[i + 1]);
                }
                else
                {
                    error("Error : alignement set multiple times");
                }

            }
            else if (arg == "-fb" || arg == "--final-branch") {
                if (atoi(argv[i + 1]) >= 30 || atoi(argv[i + 1]) < 0)
                {
                    error("Error : Final branch must be in [0, 29]");
                }
                branch_tab[atoi(argv[i + 1])] = 2;
            }
            else if (arg == "-v" || arg == "--verbose")
            {
                if (!check[3])
                {
                    if ((atoi(argv[i + 1]) < 0) && (atoi(argv[i + 1]) > 5))
                    {
                        error("Error : verbose level can only be between 0 and 5");
                    }
                    check[3] = 1;
                    verbose_level = atoi(argv[i + 1]);
                }
                else
                {
                    error("Error : verbose level set multiple times");
                }

            }
            else
            {
                error("Error : " + arg + " is not a valid parameter");
            }

            i++;
        }
    }

    if (file == "")
        error("Error : file must be set");

    struct stat buffer;
    if (!(stat(file.c_str(), &buffer) == 0))
        error("Error : file does not exist");

    Program prog(file);
    Mips * mips = new Mips(&prog, branch_tab, !alignement, verbose_level);
    mips->set_mips_stages();
    mips->do_cycle();
    EmulProg * emul = mips->get_emul_prog();
    Svgss2 * drawing = new Svgss2(output, emul->get_cycle(), emul->get_inst(), emul->get_h());
    drawing->output(emul);
    delete drawing;
    return 0;
}
