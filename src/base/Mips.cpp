#include <stdlib.h>
#include <stdio.h>

#include "Mips.h"
#include "Stages.h"

#define NB_INST_MAX_ 100
#define MAX_BRANCH   30
#define nb_stages_ 8

#define INST_NAME(x) (op_profile[(get_prog()->get_instructions()[x])->getope()].nom)
#define INST_TYPE(x) (op_profile[(get_prog()->get_instructions()[x])->getope()].type)
#define EMUL_PROG    (get_emul_prog())


Mips::Mips(Program * prog, int * branch, int alignement, int verbose)
{
    int i = 0;
    alignement_ = alignement;
    cycle_ = 0;
    nb_free_slot_ = 4;
    emul_ = new EmulProg(verbose);
    prog_ = prog;
    branch_ = branch;
    verbose_ = verbose;
    pc_ = 0;
    nextpc_ = -1;
    branchi_ = 0;
    final_branch_ = -1;
    final_inst_ = -1;
    final_branch_state_ = NONE;
    final_fill_buf_ = 0;

    Dinst_ = new InstData *[NB_INST_MAX_];
    Dinst_[i++] = new InstData("add", ProdE, ConsE, ConsE);
    Dinst_[i++] = new InstData("sub", ProdE, ConsE, ConsE);
    Dinst_[i++] = new InstData("addu", ProdE, ConsE, ConsE);
    Dinst_[i++] = new InstData("subu", ProdE, ConsE, ConsE);

    Dinst_[i++] = new InstData("addi", ProdE, ConsE, NotAnOp);
    Dinst_[i++] = new InstData("addiu", ProdE, ConsE, NotAnOp);

    Dinst_[i++] = new InstData("or", ProdE, ConsE, ConsE);
    Dinst_[i++] = new InstData("and", ProdE, ConsE, ConsE);
    Dinst_[i++] = new InstData("xor", ProdE, ConsE, ConsE);
    Dinst_[i++] = new InstData("nor", ProdE, ConsE, ConsE);

    Dinst_[i++] = new InstData("ori", ProdE, ConsE, NotAnOp);
    Dinst_[i++] = new InstData("andi", ProdE, ConsE, NotAnOp);
    Dinst_[i++] = new InstData("xori", ProdE, ConsE, NotAnOp);

    Dinst_[i++] = new InstData("sllv", ProdE, ConsE, ConsE);
    Dinst_[i++] = new InstData("srlv", ProdE, ConsE, ConsE);
    Dinst_[i++] = new InstData("srav", ProdE, ConsE, ConsE);
    Dinst_[i++] = new InstData("sll", ProdE, ConsE, ConsE);
    Dinst_[i++] = new InstData("srl", ProdE, ConsE, ConsE);
    Dinst_[i++] = new InstData("sra", ProdE, ConsE, ConsE);

    Dinst_[i++] = new InstData("lui", ProdE, NotAnOp, NotAnOp);

    Dinst_[i++] = new InstData("slt", ProdE, ConsE, ConsE);
    Dinst_[i++] = new InstData("sltu", ProdE, ConsE, ConsE);
    Dinst_[i++] = new InstData("slti", ProdE, ConsE, NotAnOp);
    Dinst_[i++] = new InstData("sltiu", ProdE, ConsE, NotAnOp);

    Dinst_[i++] = new InstData("mult", ConsE, ConsE, NotAnOp);
    Dinst_[i++] = new InstData("multu", ConsE, ConsE, NotAnOp);
    Dinst_[i++] = new InstData("div", ConsE, ConsE, NotAnOp);
    Dinst_[i++] = new InstData("divu", ConsE, ConsE, NotAnOp);

    Dinst_[i++] = new InstData("mfhi", ProdE, NotAnOp, NotAnOp);
    Dinst_[i++] = new InstData("mflo", ProdE, NotAnOp, NotAnOp);
    Dinst_[i++] = new InstData("mthi", ConsE, NotAnOp, NotAnOp);
    Dinst_[i++] = new InstData("mtlo", ConsE, NotAnOp, NotAnOp);

    Dinst_[i++] = new InstData("lw", ProdM, NotAnOp, ConsE);
    Dinst_[i++] = new InstData("lh", ProdM, NotAnOp, ConsE);
    Dinst_[i++] = new InstData("lhu", ProdM, NotAnOp, ConsE);
    Dinst_[i++] = new InstData("lb", ProdM, NotAnOp, ConsE);
    Dinst_[i++] = new InstData("lbu", ProdM, NotAnOp, ConsE);
    Dinst_[i++] = new InstData("sw", ConsE, NotAnOp, ConsE);
    Dinst_[i++] = new InstData("sh", ConsE, NotAnOp, ConsE);
    Dinst_[i++] = new InstData("sb", ConsE, NotAnOp, ConsE);

    Dinst_[i++] = new InstData("beq", ConsD, ConsD, NotAnOp);
    Dinst_[i++] = new InstData("bne", ConsD, ConsD, NotAnOp);

    Dinst_[i++] = new InstData("bgez",   ConsD, NotAnOp, NotAnOp);
    Dinst_[i++] = new InstData("bgtz",   ConsD, NotAnOp, NotAnOp);
    Dinst_[i++] = new InstData("blez",   ConsD, NotAnOp, NotAnOp);
    Dinst_[i++] = new InstData("bltz",   ConsD, NotAnOp, NotAnOp);
    Dinst_[i++] = new InstData("bgezal", ConsD, NotAnOp, NotAnOp);
    Dinst_[i++] = new InstData("bltzal", ConsD, NotAnOp, NotAnOp);

    Dinst_[i++] = new InstData("j", NotAnOp, NotAnOp, NotAnOp);
    Dinst_[i++] = new InstData("jal", NotAnOp, NotAnOp, NotAnOp);
    // jr jalr : not supported.

    nb_insts_ = i;

    buffer_[0] = -2;
    buffer_[1] = -2;
    buffer_[2] = -2;
    buffer_[3] = -2;

    // Stages creation
    stages_ = new Stages*[nb_stages_];
    // I
    stages_[0] = new Stages((char *) "mI", 0, 0, 0);
    // D
    stages_[1] = new Stages((char *) "mD", 1, 1, 1);
    // E
    stages_[2] = new Stages((char *) "E1", 1, 0, 2);
    stages_[3] = new Stages((char *) "E0", 1, 0, 2);
    // M
    stages_[4] = new Stages((char *) "M1", 0, 0, 4);
    stages_[5] = new Stages((char *) "M0", 0, 0, 4);
    // W
    stages_[6] = new Stages((char *) "W1", 0, 0, 6);
    stages_[7] = new Stages((char *) "W0", 0, 0, 6);
}

void Mips::display_stages(int cycle)
{
    if (verbose_ == 0)
    {
        return;
    }
    if (cycle == 0)
    {
        std::cout << "  I  |  D  | E0  | E1  | M0  | M1  | W0  | W1  " << std::endl;
    }
    string i, d, e0, e1, m0, m1, w0, w1;
    int inst = stages_[0]->get_inst();
    i = inst == -1 ? "none" : inst == -2 ? "nop*" : INST_NAME(inst);
    inst = stages_[1]->get_inst();
    d = inst == -1 ? "none" : inst == -2 ? "nop*" : INST_NAME(inst);
    inst = stages_[3]->get_inst();
    e0 = inst == -1 ? "none" : inst == -2 ? "nop*" : INST_NAME(inst);
    inst = stages_[2]->get_inst();
    e1 = inst == -1 ? "none" : inst == -2 ? "nop*" : INST_NAME(inst);
    inst = stages_[5]->get_inst();
    m0 = inst == -1 ? "none" : inst == -2 ? "nop*" : INST_NAME(inst);
    inst = stages_[4]->get_inst();
    m1 = inst == -1 ? "none" : inst == -2 ? "nop*" : INST_NAME(inst);
    inst = stages_[7]->get_inst();
    w0 = inst == -1 ? "none" : inst == -2 ? "nop*" : INST_NAME(inst);
    inst = stages_[6]->get_inst();
    w1 = inst == -1 ? "none" : inst == -2 ? "nop*" : INST_NAME(inst);
    for (int ind = i.size(); ind < 5; ind++)
        i += " ";
    i += "|";
    for (int ind = d.size(); ind < 5; ind++)
        d += " ";
    d += "|";
    for (int ind = e0.size(); ind < 5; ind++)
        e0 += " ";
    e0 += "|";
    for (int ind = e1.size(); ind < 5; ind++)
        e1 += " ";
    e1 += "|";
    for (int ind = m0.size(); ind < 5; ind++)
        m0 += " ";
    m0 += "|";
    for (int ind = m1.size(); ind < 5; ind++)
        m1 += " ";
    m1 += "|";
    for (int ind = w0.size(); ind < 5; ind++)
        w0 += " ";
    w0 += "|";
    for (int ind = w1.size(); ind < 5; ind++)
        w1 += " ";
    std::cout << i + d + e0 + e1 + m0 + m1 + w0 + w1 << std::endl;
}

int Mips::check_arch_error(void)
{
    Instruction ** inst = prog_->get_instructions();
    int nb_lines = prog_->get_nb_insts();
    int delayed_slot = 0;
    int inst_type;
    for (int i = 0; i < nb_lines; i++)
    {
        inst_type = op_profile[(inst[i])->getope()].type;
        if (inst_type == BR)
        {
            if (delayed_slot)
            {
                std::cout << "Error : Branchement (instruction number" << i + 1 << ") in delayed slot(s)... " << std::endl;
                return 0;
            }
            else
                delayed_slot = 1;
        }
        else
        {
            if (delayed_slot != 0)
                delayed_slot--;
        }
    }
    if (delayed_slot)
    {
        std::cout << "Error : Empty delayed slot(s)... " << std::endl;
        return 0;
    }
    return 1;
}

void Mips::set_mips_stages(void)
{
    for (int i = 0; i < nb_stages_ ; i++)
        stages_[i]->set_mips(this);
}

void Mips::display_InstData(void)
{
    for(int i = 0; i < nb_insts_ ; i++)
        Dinst_[i]->display();
}

void Mips::set_pc(int pc)
{
    pc_ = pc;
}

void Mips::set_nextpc(int nextpc)
{
    nextpc_ = nextpc;
}

int Mips::get_branch(void)
{
    return branch_[branchi_++];
}

int Mips::get_pc(void)
{
    return pc_;
}

int Mips::get_stage_old_inst(int stage)
{
    return stages_[stage]->get_old_inst();
}

int Mips::get_stage_inst(int stage)
{
    return stages_[stage]->get_inst();
}

int Mips::get_nb_stages(void)
{
    return nb_stages_;
}

int Mips::get_stage(char c)
{
    switch(c)
    {
        case 'I' :
            return 0;
        case 'D' :
            return 1;
        case 'E' :
            return 2;
        case 'M' :
            return 4;
        case 'W' :
            return 6;
        default :
            printf("In Mips::get_stage, wrong value for char");
            exit(-1);
    }
}

void Mips::fill_prod_reg(int prod_reg_p[][2], int inst)
{
    if (inst < 0)
        return;
    int * prod_reg_ = *prod_reg_p;
    Instruction * instr = (prog_->get_instructions())[inst];
    string inst_name = op_profile[instr->getope()].nom;
    string ** data;
    if (inst_name == "jal" || inst_name == "bgezal" || inst_name == "bltzal")
    {
        prod_reg_[0] = 31;
        prod_reg_[1] = get_stage('D');
        return;
    }
    if (inst_name == "mtlo")
    {
        prod_reg_[0] = 32;
        prod_reg_[1] = get_stage('D');
        return;
    }
    if (inst_name == "mthi")
    {
        prod_reg_[0] = 33;
        prod_reg_[1] = get_stage('D');
        return;
    }
    if (inst_name == "div" || inst_name == "divu" || inst_name == "mult" || inst_name == "multu")
    {
        prod_reg_[0] = 34;
        prod_reg_[1] = 'W';
        return;
    }
    for (int i = 0; i < nb_insts_; i++)
    {
        if (inst_name == Dinst_[i]->get_name())
        {
            data = Dinst_[i]->get_data();
            break;
        }
    }
    if (data[0][0] != "" && data[0][0] != "X")
    {
        prod_reg_[0] = instr->get_reg_number(0);
        prod_reg_[1] = get_stage(data[0][0].at(0));
    }
    else if (data[1][0] != "" && data[1][0] != "X")
    {
        prod_reg_[0] = instr->get_reg_number(1);
        prod_reg_[1] = get_stage(data[1][0].at(0));
    }
    else if (data[2][0] != "" && data[2][0] != "X")
    {
        prod_reg_[0] = instr->get_reg_number(2);
        prod_reg_[1] = get_stage(data[2][0].at(0));
    }
    else
    {
        prod_reg_[0] = -1;
        prod_reg_[1] = -1;
    }
}

void Mips::fill_cons_reg(int cons_reg[][2], int inst_nb)
{
    Instruction * instr = (prog_->get_instructions())[inst_nb];
    string inst_name = op_profile[instr->getope()].nom;
    if (inst_name == "mflo")
    {
        cons_reg[0][0] = 32; // LO
        cons_reg[1][0] = get_stage('E');
        return;
    }
    if (inst_name == "mfhi")
    {
        cons_reg[0][0] = 33; //  HI
        cons_reg[1][0] = get_stage('E');
        return;
    }
    string ** data;
    for (int i = 0; i < nb_insts_; i++)
    {
        if (inst_name == Dinst_[i]->get_name())
        {
            data = Dinst_[i]->get_data();
            break;
        }
    }
    int j = 0;
    if (data[0][1] != "" && data[0][1] != "X")
    {
        cons_reg[0][j] = instr->get_reg_number(0);
        cons_reg[1][j] = get_stage(data[0][1].at(0));// only on the first
        j++;
    }
    if (data[1][1] != "" && data[1][1] != "X")
    {
        cons_reg[0][j] = instr->get_reg_number(1);
        cons_reg[1][j] = get_stage(data[1][1].at(0));
        j++;
    }
    if (data[2][1] != "" && data[2][1] != "X")
    {
        cons_reg[0][j] = instr->get_reg_number(2);
        cons_reg[1][j] = get_stage(data[2][1].at(0));
        j++;
    }
}

int Mips::get_buf_i(int i)
{
    return buffer_[i];
}

int Mips::pop_buf()
{
    int ret = buffer_[0];
    buffer_[0] = buffer_[1];
    buffer_[1] = buffer_[2];
    buffer_[2] = buffer_[3];
    buffer_[3] = -2;
    nb_free_slot_++;
    return ret;
}

void Mips::clr3_buf(void)
{
    if (buffer_[1] != -2)
        nb_free_slot_ += 1;
    if (buffer_[2] != -2)
        nb_free_slot_ += 1;
    if (buffer_[3] != -2)
        nb_free_slot_ += 1;
    buffer_[1] = -2;
    buffer_[2] = -2;
    buffer_[3] = -2;
}

int Mips::fill_buffer(void)
{
    if (nextpc_ != -1)
    {
        pc_ = nextpc_;
        nextpc_ = -1;
        return -1;
    }
    int is_final = final_fill_buf_;
    if (nb_free_slot_ < 2)
        return 0;
    int i_free_slot = 4 - nb_free_slot_;
    if (!((pc_ + alignement_) % 2))
    {
        for (int i = 0; i < 2; i++)
        {
            if (pc_ + i >= prog_->get_nb_insts() || is_final == 1) // not an inst
                buffer_[i_free_slot] = -3;
            else
            {
                buffer_[i_free_slot] = pc_ + i;
                if (final_branch_state_ != NONE)
                {
                    if (pc_ + i == final_inst_)
                        final_fill_buf_ = 1;
                }
            }
            i_free_slot++;
        }
        nb_free_slot_ -= 2;
        pc_ += 2;
    }
    else
    {
        if (pc_ >= prog_->get_nb_insts() || is_final == 1) // not an inst
            buffer_[i_free_slot] = -3;
        else
        {
            buffer_[i_free_slot] = pc_;
            if (final_branch_state_ != NONE)
            {
                if (pc_ == final_inst_)
                    final_fill_buf_ = 1;
            }
        }
        nb_free_slot_ -= 1;
        pc_ += 1;
    }
    return !is_final;
}

void Mips::emul_add_inst(int inst)
{
    if (is_nop(inst))
    {
        emul_->add_inst("nop");
        return;
    }
    emul_->add_inst(INST_NAME(inst));
}

int Mips::set_inst_stage(int inst, int stage)
{
    return stages_[stage]->set_inst(inst);
}

EmulProg * Mips::get_emul_prog(void)
{
    return emul_;
}

void Mips::set_final(int br, int inst)
{
    final_branch_state_ = WAITING_FINAL_BRANCH;
    final_branch_ = br;
    final_inst_ = inst;
}

int Mips::check_end(void)
{
    if (final_branch_state_ == WAITING_FINAL_BRANCH)
    {
        if (get_stage_old_inst(6) == final_branch_)
            final_branch_state_ = WAITING_FINAL_INST;
        if (get_stage_old_inst(7) == final_branch_)
            final_branch_state_ = WAITING_FINAL_INST;
    }
    else if (final_branch_state_ == WAITING_FINAL_INST)
    {
        if (get_stage_old_inst(6) == final_inst_)
            return 1;
        if (get_stage_old_inst(7) == final_inst_)
            return 1;
    }


    if (cycle_ == MAX_CYCLE)
    {
        return 1;
    }

    for (int i = 0; i < nb_stages_; i++)
    {
        if (stages_[i]->get_inst() != -1)
            return 0;
    }

    if (buffer_[0] < 0)
    {
        return 1;
    }

    return 0;
}

void Mips::set_next_emul_inst(int stage_nb, int einst)
{
    stages_[stage_nb]->set_next_emul_inst(einst);
}

void Mips::set_emul_inst(int stage_nb, int einst)
{
    stages_[stage_nb]->set_emul_inst(einst);
}

int Mips::get_emul_inst(int stage_nb)
{
    return stages_[stage_nb]->get_emul_inst();
}

int Mips::get_old_emul_inst(int stage_nb)
{
    return stages_[stage_nb]->get_old_emul_inst();
}

int Mips::get_cycle(void)
{
    return cycle_;
}

Program * Mips::get_prog(void)
{
    return prog_;
}

int Mips::check_code(void)
{
    Instruction ** inst = prog_->get_instructions();
    int nb_lines = prog_->get_nb_insts();
    int found = 0;
    int error = 0;
    string inst_name;
    for (int i = 0; i < nb_lines; i++)
    {
        inst_name = op_profile[(inst[i])->getope()].nom;
        found = 0;
        for (int j = 0; j < nb_insts_; j++)
        {
            if (inst_name == Dinst_[j]->get_name())
            {
                found = 1;
                break;
            }
        }
        if (found == 0)
        {
            std::cout << "error : instuction " << inst_name << " is not a valid instruction" << std::endl;
            error = 1;
        }
    }
    return error;
}

int Mips::is_nop(int inst)
{
    if (INST_NAME(inst) == "add")
    {
        if ((prog_->get_instructions()[inst])->get_reg_number(0) == 0)
        {
            if ((prog_->get_instructions()[inst])->get_reg_number(1) == 0)
            {
                if ((prog_->get_instructions()[inst])->get_reg_number(2) == 0)
                {
                    return 1;
                }
            }
        }
    }
    return 0;
}

void Mips::write_buf(void)
{
    string i1, i2, i3, i4;
    if (buffer_[0] == -2)
        i1 = "nop*";
    else if (buffer_[0] == -3)
        i1 = "X";
    else if (is_nop(buffer_[0]))
        i1 = "nop";
    else
        i1 = INST_NAME(buffer_[0]);

    if (buffer_[1] == -2)
        i2 = "nop*";
    else if (buffer_[1] == -3)
        i2 = "X";
    else if (is_nop(buffer_[1]))
        i2 = "nop";
    else
        i2 = INST_NAME(buffer_[1]);

    if (buffer_[2] == -2)
        i3 = "nop*";
    else if (buffer_[2] == -3)
        i3 = "X";
    else if (is_nop(buffer_[2]))
        i3 = "nop";
    else
        i3 = INST_NAME(buffer_[2]);

    if (buffer_[3] == -2)
        i4 = "nop*";
    else if (buffer_[3] == -3)
        i4 = "X";
    else if (is_nop(buffer_[3]))
        i4 = "nop";
    else
        i4 = INST_NAME(buffer_[3]);

    emul_->new_buf(i1, i2, i3, i4);
}

int Mips::get_shift_state(int i)
{
    return shift_states_[i];
}

int Mips::do_cycle(void)
{
    int end = 0;
    volatile int stop = 0;
    if (check_code())
        exit(-1);
    if (!check_arch_error())
        exit(-1);
    if (!prog_->check_labels())
        exit(-1);

    if (verbose_ > 0) {
        cout << "### Display Stages ###" << endl;
    }
    while (end != 1)
    {
        for (int i = 0; i < nb_stages_; i++)
            stages_[i]->verif_bypass(0);
        for (int i = nb_stages_ - 1; i > -1; i--)
        {
            if ((shift_states_[i] = stages_[i]->shift()) && i == 0)
                emul_->inc_h();
        }
        emul_->inc_cycle();
        write_buf();
        display_stages(cycle_);
        cycle_++;
        if (check_end())
        {
            end = 1;
        }
        if (stop == 8)
            stop++;
        else
            stop++;

    }
    emul_->display();
    return 0;
}

