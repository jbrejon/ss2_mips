#include "Stages.h"
#include "Mips.h"

#define INST_NAME(x) (op_profile[((mips_->get_prog())->get_instructions()[x])->getope()].nom)
#define INST_TYPE(x) (op_profile[((mips_->get_prog())->get_instructions()[x])->getope()].type)
#define EMUL_PROG    (mips_->get_emul_prog())

#define NOPSTAR -2

Stages::Stages(char * name, int bypass, int calc_addr, int stage_number)
{
    name_ = std::string(name);
    bypass_ = bypass;
    instruction_ = -1;
    old_inst_ = -1;
    inst_ready_ = 0;
    calc_addr_ = calc_addr;
    stage_number_ = stage_number;
    emul_inst_ = -1;
    next_emul_inst_ = -1;
    last_cycle_was_a_reset = 0;
    old_emul_inst_ = -1;
    if (name_ == "mD" || name_ == "mI")
        emul_inst_ = 0;
}

int Stages::verif_bypass(int fromshift)
{
    if (instruction_ == -1)
        return 0;
    int pregw0[2] = {-1 , -1};
    int pregw1[2] = {-1 , -1};
    if (bypass_ == 0 || instruction_ == NOPSTAR)
    {
        if (instruction_ > 0)
        {
            if ((name_.at(0) == 'M') && (INST_TYPE(instruction_) == MEM))
            {
                if (name_.at(1) == 0)
                {
                    int inst_m1 = mips_->get_stage_inst(4);
                    if (INST_TYPE(inst_m1) == MEM)
                    {
                        if (inst_m1 < instruction_)
                        {
                            inst_ready_ = 0;
                            return 0;
                        }
                    }
                }
                else
                {
                    int inst_m0 = mips_->get_stage_inst(5);
                    if (INST_TYPE(inst_m0) == MEM)
                    {
                        if (inst_m0 < instruction_)
                        {
                            inst_ready_ = 0;
                            return -1;
                        }
                    }
                }
            }
            else if (name_.at(0) == 'W')
            {
                mips_->fill_prod_reg(&pregw0, mips_->get_stage_inst(7));
                mips_->fill_prod_reg(&pregw1, mips_->get_stage_inst(6));
                int instw0 = mips_->get_stage_inst(7);
                int instw1 = mips_->get_stage_inst(6);
                if ((pregw0[0] > 0) && (pregw0[0] == pregw1[0]))
                {
                    if (name_.at(1) == '0')
                    {
                        if (instw1 < instruction_)
                        {
                            inst_ready_ = 0;
                            return -1;
                        }
                    }
                    else
                    {
                        if (instw0 < instruction_)
                        {
                            inst_ready_ = 0;
                            return -1;
                        }
                    }
                }
            }
        }
        inst_ready_ = 1;
        return 0;
    }
    //    int * prod_reg = mips_->get_prod_reg();
    int nb_stages = mips_->get_nb_stages();
    int ok = -1;
    int mflohi = 0;
    int done[2] = {0 ,0};
    int prod_reg[2] = { -1, -1 };
    int cons_reg[2][2] = { { -1, -1 }, { -1, -1} };
    int start = 0;
    int shft = 0;
    int (Mips::*get_inst_s) (int) = &Mips::get_stage_inst;

    if (fromshift)
        get_inst_s = &Mips::get_stage_old_inst;

    if (name_ == "mD")
        start = 2;
    else
    {
        start = 4;
        if (name_.at(1) == '1')
            shft = 1;
    }
    /* #####     cons reg    ######
       #        0           1     #[][x]
       # 0 |nb_reg_op1|nb_reg_op2|#
       # 1 |cons_stage|cons_stage|#
       #[y][]                     #
       ############################*/

    mips_->fill_cons_reg(cons_reg, instruction_);
    if (cons_reg[0][0] == 32 || cons_reg[0][0] == 33) // mfhi or mflo
        mflohi = 1;

    for (int i = start; i < nb_stages; i++)
    {
        int m = 0;
        if (stage_number_ == 1)
            m = 1;
        if (i == 2 || i == 3)
            m = 2;
        if ((stage_number_ == 1) && ((i == 2) || (i == 3)))
            m = 3;
        mips_->fill_prod_reg(&prod_reg, (mips_->*get_inst_s)(i));
        if (cons_reg[0][0] > 0 && done[0] == 0)
        {
            if (prod_reg[0] == cons_reg[0][0] && stage_number_ <= cons_reg[1][0])
            {
                if ((i - i % 2) >= prod_reg[1] + 1)
                {
                    if (fromshift)
                        (mips_->get_emul_prog())->add_bypass(mips_->get_cycle(), emul_inst_ * 2 + shft, mips_->get_old_emul_inst(i) * 2 + !(i % 2), 0, m);
                    else
                        (mips_->get_emul_prog())->add_bypass(mips_->get_cycle(), emul_inst_ * 2 + shft, mips_->get_emul_inst(i) * 2 + !(i % 2), 0, m);
                    if (ok == -1)
                        ok = 1;
                }
                else if (cons_reg[1][0] > stage_number_)
                {
                    if (ok == -1)
                        ok = 1;
                }
                else
                    ok = 0;
                done[0] = 1;
            }
            else if (mflohi && prod_reg[0] == 34)
            {
                if (mips_->get_stage('E') > stage_number_)
                    ok = 1;
                else
                    ok = 0;
                done[0] = 1;
            }
        }
        if (cons_reg[0][1] > 0 && done[1] == 0)
        {
            if (prod_reg[0] == cons_reg[0][1] && stage_number_ <= cons_reg[1][1])
            {
                if ((i - i % 2) >= prod_reg[1] + 1)
                {
                    if (fromshift)
                        (mips_->get_emul_prog())->add_bypass(mips_->get_cycle(), emul_inst_ * 2 + shft, mips_->get_old_emul_inst(i) * 2 + !(i % 2), 1, m);
                    else
                        (mips_->get_emul_prog())->add_bypass(mips_->get_cycle(), emul_inst_ * 2 + shft, mips_->get_emul_inst(i) * 2 + !(i % 2), 1, m);
                    if (ok == -1)
                        ok = 1;
                    done[1] = 1;
                }
                else if (cons_reg[1][1] > stage_number_)
                {
                    if (ok == -1)
                        ok = 1;
                    done[1] = 1;
                }
                else
                {
                    ok = 0;
                    done[1] = 1;
                }
            }
        }
    }
    if (ok == 1 || ok == -1)
        inst_ready_ = 1;
    else
        inst_ready_ = 0;
    return 0;
}

int Stages::exec(int inst)
{
    if (inst == -1 || inst == -2)
        return 0;
    // Branchement handling
    string inst_name = INST_NAME(inst);
    if (calc_addr_ == 1 && (inst_name == "j" || inst_name == "jal"))
        mips_->set_nextpc((mips_->get_prog())->get_label(((mips_->get_prog())->get_instructions())[inst]->get_label()));
    else if (calc_addr_ == 1 && INST_TYPE(inst) == BR && inst_ready_ == 1) {
        int branch = mips_->get_branch();
        if (branch == 2)
        {
            mips_->set_final(inst, mips_->get_prog()->get_label(mips_->get_prog()->get_instructions()[inst]->get_label()));
        }
        if (branch == 1 || branch == 2)
            mips_->set_nextpc((mips_->get_prog())->get_label(((mips_->get_prog())->get_instructions())[inst]->get_label()));
        else
            return 0;
    }
    else
        return 0;
    return 1;
}

int Stages::shift(void) //W first
{
    if (name_ == "mI")
    {
        int ret = mips_->fill_buffer();

        if (ret == 1) //success
        {
            EMUL_PROG->add_data(name_, emul_inst_ * 2);
            if ((mips_->get_shift_state(1) == 0) || (mips_->get_cycle() == 0))
                emul_inst_++;
            else if (last_cycle_was_a_reset == 1)
                emul_inst_++;
            return 1;
        }
        else if (ret == -1) //reset buffer
        {
            //emul_inst_++;
            last_cycle_was_a_reset = 1;
        }
        else //failed (ret == 0)
        {
            if (mips_->get_shift_state(1) == 0)
                emul_inst_++;
        }
        return 0;
    }
    else if (name_.at(0) == 'W')
    {
        old_inst_ = instruction_;
        old_emul_inst_ = emul_inst_;
        if (instruction_ != -1)
        {
            if (name_.at(1) == '0')
            {
                if (inst_ready_ == 0)
                {
                    EMUL_PROG->add_data("C", emul_inst_ * 2);
                    return -1;
                }
                EMUL_PROG->add_data(name_, emul_inst_ * 2);
            }
            else
            {
                if (inst_ready_ == 0)
                {
                    EMUL_PROG->add_data("C", emul_inst_ * 2 + 1);
                    return -1;
                }
                EMUL_PROG->add_data(name_, emul_inst_ * 2 + 1);
            }
        }
        instruction_ = -1;
        inst_ready_ = 0;
        return 0;
    }
    else if (name_ == "mD")
    {
        int inst1 = mips_->get_buf_i(0);
        int inst2 = mips_->get_buf_i(1);
        int prod_reg[2];
        int cons_reg[2][2] = {{ -1, -1 }, { -1, -1}};
        if (inst1 == -3)
            return -1;

        if (inst1 == NOPSTAR)
            return -1;

        if (inst2 == NOPSTAR || inst2 == -3)
        {
            if (INST_TYPE(inst1) == BR)
            {
                EMUL_PROG->add_data("mC", emul_inst_ * 2);
                return -1;
            }
            if (mips_->set_inst_stage(inst1, stage_number_ + 2) == -1)
            {
                if (mips_->set_inst_stage(inst1, stage_number_ + 1) != -1)
                {
                    EMUL_PROG->add_inst("   ");
                    mips_->set_next_emul_inst(stage_number_ + 2, mips_->get_emul_inst(stage_number_ + 2) + 1);
                    inst1 = mips_->pop_buf();
                    mips_->emul_add_inst(inst1);
                    EMUL_PROG->add_data("mD", emul_inst_ * 2);
                    emul_inst_++;
                    return 0;
                }
                else
                    EMUL_PROG->add_data("mC", emul_inst_ * 2);
                return -1;
            }
            EMUL_PROG->add_data("mD", emul_inst_ * 2);
            emul_inst_++;
            mips_->emul_add_inst(mips_->pop_buf());
            if (mips_->set_inst_stage(NOPSTAR, stage_number_ + 1) == -1)
            {
                EMUL_PROG->add_inst("   ");
                return 0;
            }
            EMUL_PROG->add_inst("nop*");
            return 0;
        }

        // dependencies
        mips_->fill_prod_reg(&prod_reg, inst1);
        mips_->fill_cons_reg(cons_reg, inst2);
        if (    ((prod_reg[0] != 0) && (prod_reg[0] != -1)) &&
                ((cons_reg[0][0] == prod_reg[0]) ||
                 (cons_reg[0][1] == prod_reg[0]) ||
                 ((cons_reg[0][0] == 34) && ((prod_reg[0] == 33) || (prod_reg[0] == 32)))))
        {
            if (INST_TYPE(inst1) == BR)
            {
                if (branch_handling(inst1, stage_number_ + 2) == 0)
                {
                    EMUL_PROG->add_data("mC", emul_inst_ * 2);
                    return -1;
                }
                inst1 = mips_->pop_buf();
                if (exec(inst1))
                    mips_->clr3_buf();
            }
            else
            {
                if (mips_->set_inst_stage(inst1, stage_number_ + 2) == -1)
                {
                    if (mips_->set_inst_stage(inst1, stage_number_ + 1) != -1)
                    {
                        EMUL_PROG->add_inst("   ");
                        mips_->set_next_emul_inst(stage_number_ + 2, mips_->get_emul_inst(stage_number_ + 2) + 1);
                        inst1 = mips_->pop_buf();
                        mips_->emul_add_inst(inst1);
                        EMUL_PROG->add_data("mD", emul_inst_ * 2);
                        emul_inst_++;
                        return 0;
                    }
                    else
                        EMUL_PROG->add_data("mC", emul_inst_ * 2);
                    return -1;
                }
                inst1 = mips_->pop_buf();
            }
            EMUL_PROG->add_data("mD", emul_inst_ * 2);
            emul_inst_++;
            mips_->emul_add_inst(inst1);
            if (mips_->set_inst_stage(NOPSTAR, stage_number_ + 1) == -1)
            {
                EMUL_PROG->add_inst("   ");
                mips_->set_next_emul_inst(stage_number_ + 1, mips_->get_emul_inst(stage_number_ + 1) + 1);
                return 0;
            }
            EMUL_PROG->add_inst("nop*");
            return 0;
        }

        // inst2 == BR
        if (INST_TYPE(inst2) == BR && mips_->get_buf_i(2) < 0)
        {
            if (mips_->set_inst_stage(inst1, stage_number_ + 2) == -1)
            {
                if (mips_->set_inst_stage(inst1, stage_number_ + 1) != -1)
                {
                    EMUL_PROG->add_inst("   ");
                    mips_->set_next_emul_inst(stage_number_ + 2, mips_->get_emul_inst(stage_number_ + 2) + 1);
                    inst1 = mips_->pop_buf();
                    mips_->emul_add_inst(inst1);
                    EMUL_PROG->add_data("mD", emul_inst_ * 2);
                    emul_inst_++;
                    return 0;
                }
                else
                    EMUL_PROG->add_data("mC", emul_inst_ * 2);
                return -1;
            }
            EMUL_PROG->add_data("mD", emul_inst_ * 2);
            emul_inst_++;
            mips_->emul_add_inst(mips_->pop_buf());
            if (mips_->set_inst_stage(NOPSTAR, stage_number_ + 1) == -1)
            {
                EMUL_PROG->add_inst("   ");
                mips_->set_next_emul_inst(stage_number_ + 1, mips_->get_emul_inst(stage_number_ + 1) + 1);
                return 0;
            }
            EMUL_PROG->add_inst("nop*");
            return 0;
        }

        if (INST_TYPE(inst1) == BR)
        {
            if (branch_handling(inst1, stage_number_ + 2) == 0)
            {
                EMUL_PROG->add_data("mC", emul_inst_ * 2);
                return -1;
            }
            inst1 = mips_->pop_buf();
            mips_->set_inst_stage(inst1, stage_number_ + 2);
            if (exec(inst1))
                mips_->clr3_buf();
        }
        else //normal case
        {
            if (mips_->set_inst_stage(inst1, stage_number_ + 2) == -1)
            {
                if (mips_->set_inst_stage(inst1, stage_number_ + 1) != -1)
                {
                    EMUL_PROG->add_inst("   ");
                    mips_->set_next_emul_inst(stage_number_ + 2, mips_->get_emul_inst(stage_number_ + 2) + 1);
                    inst1 = mips_->pop_buf();
                    mips_->emul_add_inst(inst1);
                    EMUL_PROG->add_data("mD", emul_inst_ * 2);
                    emul_inst_++;
                    return 0;
                }
                else
                    EMUL_PROG->add_data("mC", emul_inst_ * 2);
                return -1;
            }
            inst1 = mips_->pop_buf();
        }
        EMUL_PROG->add_data("mD", emul_inst_ * 2);
        mips_->emul_add_inst(inst1);

        if (INST_TYPE(inst2) == BR)
        {
            if (branch_handling(inst2, stage_number_ + 1) == 0)
            {
                if (mips_->set_inst_stage(NOPSTAR, stage_number_ + 1) == -1)
                {
                    EMUL_PROG->add_inst("   ");
                    mips_->set_next_emul_inst(stage_number_ + 1, mips_->get_emul_inst(stage_number_ + 1) + 1);
                    emul_inst_++;
                    return 0;
                }
                EMUL_PROG->add_inst("nop*");
                emul_inst_++;
                return 0;
            }
            inst2 = mips_->pop_buf();
            mips_->set_inst_stage(inst2, stage_number_ + 1);
            if (exec(inst2))
                mips_->clr3_buf();
        }
        else
        {
            if (mips_->set_inst_stage(inst2, stage_number_ + 1) == -1)
            {
                EMUL_PROG->add_inst("   ");
                mips_->set_next_emul_inst(stage_number_ + 1, mips_->get_emul_inst(stage_number_ + 1) + 1);
                emul_inst_++;
                return 0;
            }
            inst2 = mips_->pop_buf();
        }
        emul_inst_++;
        mips_->emul_add_inst(inst2);
    }
    else // E0/1, M0/1
    {
        old_inst_ = instruction_;
        old_emul_inst_ = emul_inst_;
        int shft = 1;
        if (name_.at(1) == '0')
            shft = 0;
        if (instruction_ != -1)
        {
            if (inst_ready_ == 0)
            {
                (mips_->get_emul_prog())->add_data("C", emul_inst_ * 2 + shft);
                return -1;
            }
        }
        if (instruction_ != -1)
        {
            if ((shft == 1) &&
                    ((mips_->get_shift_state(stage_number_ + 1) == 0) || (mips_->get_emul_inst(stage_number_ + 1) > emul_inst_)))
            {
                if (mips_->set_inst_stage(instruction_, stage_number_ + !shft + 2) == -1)
                {
                    inst_ready_ = 0;
                    if (instruction_ != -1)
                        (mips_->get_emul_prog())->add_data("C", emul_inst_ * 2 + shft);
                    return -1;
                }
                else
                {
                    mips_->set_emul_inst(stage_number_ + 2 + !shft, emul_inst_);
                    (mips_->get_emul_prog())->add_data(name_, emul_inst_ * 2 + shft);
                    if (next_emul_inst_ != -1)
                        emul_inst_ = next_emul_inst_;
                    next_emul_inst_ = -1;
                }
            }
            else if (shft == 0)
            {
                if (mips_->set_inst_stage(instruction_, stage_number_ + !shft + 2) == -1)
                {
                    inst_ready_ = 0;
                    if (instruction_ != -1)
                        (mips_->get_emul_prog())->add_data("C", emul_inst_ * 2 + shft);
                    return -1;
                }
                else
                {
                    mips_->set_emul_inst(stage_number_ + 2 + !shft, emul_inst_);
                    (mips_->get_emul_prog())->add_data(name_, emul_inst_ * 2 + shft);
                    if (next_emul_inst_ != -1)
                        emul_inst_ = next_emul_inst_;
                    next_emul_inst_ = -1;
                }
            }
            else
            {
                (mips_->get_emul_prog())->add_data("C", emul_inst_ * 2 + shft);
                return -1;
            }
        }
        instruction_ = -1;
        inst_ready_ = 0;
        return 0;
    }
    return 0;
}

int Stages::set_inst(int inst)
{
    if (instruction_ != -1)
        if (inst_ready_ == 0)
            return -1;
    if (stage_number_ > 1)
    {
        if (name_.at(1) == '1')
        {
            if (instruction_ != -1)
            {
                if (!((mips_->get_shift_state(stage_number_ + 1) == 0) || (mips_->get_emul_inst(stage_number_ + 1) > emul_inst_)))
                    return -1;
            }
            else
            {
                if (!(mips_->get_emul_inst(stage_number_ + 1) >= emul_inst_))
                    return -1;
            }
        }
    }
    instruction_ = inst;
    if (instruction_ != -1)
    {
        if ((name_.at(0) == 'm') || (name_.at(0) == 'E'))
            emul_inst_++;
    }
    return 0;
}

int Stages::branch_handling(int inst, int stagetotest)
{
    if (mips_->get_stage_inst(stagetotest) != -1)
        return 0;
    instruction_ = inst;
    verif_bypass(1);
    instruction_ = -1;
    if (inst_ready_ == 0)
        return 0;
    return 1;
}

int Stages::get_inst(void)
{
    return instruction_;
}

int Stages::get_old_inst(void)
{
    return old_inst_;
}

int Stages::get_emul_inst(void)
{
    return emul_inst_;
}

int Stages::get_old_emul_inst(void)
{
    if (old_emul_inst_ < 0)
        return 0;
    return old_emul_inst_;
}

void Stages::set_emul_inst(int einst)
{
    emul_inst_ = einst;
}

void Stages::set_next_emul_inst(int einst)
{
    next_emul_inst_ = einst;
}

void Stages::set_mips(Mips * mips)
{
    mips_ = mips;
}

