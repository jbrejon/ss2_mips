#include "Instruction.h"

Instruction::Instruction(t_Operator ope, int op1, int op2, int op3)
{
    nb_op_ = 3;
    ope_ = ope;
    op_[0] = op1;
    op_[1] = op2;
    op_[2] = op3;
    label_ = "";
    expr_ = "";
}

Instruction::Instruction(t_Operator ope, int op1, int op2)
{
    nb_op_ = 2;
    ope_ = ope;
    op_[0] = op1;
    op_[1] = op2;
    op_[2] = -1;
    label_ = "";
    expr_ = "";
}

Instruction::Instruction(t_Operator ope, int op1)
{
    ope_ = ope;
    op_[0] = op1;
    op_[1] = -1;
    op_[2] = -1;
    label_ = "";
    expr_ = "";
}

Instruction::Instruction(t_Operator ope)
{
    ope_ = ope;
    op_[0] = -1;
    op_[1] = -1;
    op_[2] = -1;
    label_ = "";
    expr_ = "";
}

Instruction::Instruction(t_Operator ope, int op1, string expr, int op3)
{
    nb_op_ = 2;
    ope_ = ope;
    op_[0] = op1;
    op_[1] = -1;
    op_[2] = op3;
    label_ = "";
    expr_ = expr;
}

Instruction::Instruction(t_Operator ope, int op1, int op2, string s)
{
    nb_op_ = 2;
    ope_ = ope;
    op_[0] = op1;
    op_[1] = op2;
    op_[2] = -1;
    if (op_profile[ope].type == BR)
    {
        label_ = s;
        expr_ = "";
    }
    else
    {
        label_ = "";
        expr_ = s;
    }
}

Instruction::Instruction(t_Operator ope, int op1, string s)
{
    nb_op_ = 1;
    ope_ = ope;
    op_[0] = op1;
    op_[1] = -1;
    op_[2] = -1;
    if (op_profile[ope].type == BR)
    {
        label_ = s;
        expr_ = "";
    }
    else
    {
        label_ = "";
        expr_ = s;
    }
}


Instruction::Instruction(t_Operator ope, string s)
{
    nb_op_ = 0;
    ope_ = ope;
    op_[0] = -1;
    op_[1] = -1;
    op_[2] = -1;
    if (op_profile[ope].type == BR)
    {
        label_ = s;
        expr_ = "";
    }
    else
    {
        label_ = "";
        expr_ = s;
    }
}

Instruction::~Instruction()
{
}


t_Operator Instruction::getope(void)
{
    return ope_;
}

string Instruction::get_label(void)
{
    return label_;
}

int Instruction::get_reg_number(int op_number)
{
    if (op_number == 0)
        return op_[0];
    if (op_number == 1)
        return op_[1];
    if (op_number == 2)
        return op_[2];
    else
        return -1;

}

void Instruction::display(void)
{
    std::string str;
    std::stringstream s;
    s << op_profile[ope_].nom << " ";
    if (op_[0] != -1)
        s << op_[0] << ", ";
    if (op_[1] != -1)
        s << op_[1] << ", ";
    if (op_[2] != -1)
        s << op_[2] << ", ";
    if (label_ != "")
        s << label_ << ", ";
    if (expr_ !=  "")
        s << expr_ << ", ";
    str = s.str();
    str[str.size() - 2] = ' ';
    str[str.size() - 1] = ' ';
    std::cout << str << std::endl;
}

