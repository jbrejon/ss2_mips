#include "InstData.h"

InstData::InstData(string name, t_Data op1, t_Data op2, t_Data op3)
{
    // (opx mod 2) = 0 or 1
    dinst_ = new string*[3];
    for (int i = 0; i < 3; i++)
        dinst_[i] = new string[2];
    
    name_ = name;
    dinst_[0][op1 % 2] = Data[op1];
    dinst_[0][(op1 + 1) % 2] = "";
    dinst_[1][op2 % 2] = Data[op2];
    dinst_[1][(op2 + 1) % 2] = "";
    dinst_[2][op3 % 2] = Data[op3];
    dinst_[2][(op3 + 1) % 2] = "";
}

string InstData::get_name(void)
{
    return name_;
}

string ** InstData::get_data(void)
{
    return (string **)dinst_;
}


void InstData::display()
{
    std::cout << name_;
    if (dinst_[0][0] != "" && dinst_[0][0] != "X")
        std::cout << " produce op1 at " << dinst_[0][0];
    if (dinst_[1][0] != "" && dinst_[1][0] != "X")
        std::cout << " produce op2 at " << dinst_[1][0];
    if (dinst_[2][0] != "" && dinst_[2][0] != "X")
        std::cout << " produce op3 at " << dinst_[2][0];
    if (dinst_[0][1] != "" && dinst_[0][1] != "X")
        std::cout << " consume op1 at " << dinst_[0][1];
    if (dinst_[1][1] != "" && dinst_[1][1] != "X")
        std::cout << " consume op2 at " << dinst_[1][1];
    if (dinst_[2][1] != "" && dinst_[2][1] != "X")
        std::cout << " consume op3 at " << dinst_[2][1];
    std::cout << "" << std::endl;
}
