
#include <fstream>
#include <iostream>
#include <string>
#include <cstring>
#include "Svgss2.h"

#define MAX(x, y) ((x) > (y) ? (x) : (y))
#define MIN(x, y) ((x) < (y) ? (x) : (y))
#define UNIT_PER_PIXEL 2
#define BASE_PIXEL_X 40
#define BASE_PIXEL_Y 25
#define RESOL_COEF 1
#define PIXEL_LEFT_PAD 80


Svgss2::Svgss2(const std::string & filename, int cycle, int inst, int h)
{
    unit_size_x_ = BASE_PIXEL_X * UNIT_PER_PIXEL;
    unit_size_y_ = BASE_PIXEL_Y * UNIT_PER_PIXEL;
    left_padding_ = PIXEL_LEFT_PAD * UNIT_PER_PIXEL;
    upper_padding_ = 2 * unit_size_y_; // for the buffer
    cycle_ = cycle;
    inst_  = inst;
    h_ = h;
    width_ = left_padding_ + unit_size_x_ * cycle;
    height_ = upper_padding_ + unit_size_y_ * (MAX(h_, inst_ % 2 + inst_) + 1);
    pixel_x_ = RESOL_COEF * (PIXEL_LEFT_PAD + BASE_PIXEL_X * cycle);
    pixel_y_ = RESOL_COEF * (2 * BASE_PIXEL_Y + BASE_PIXEL_Y * (MAX(h_, inst_ % 2 + inst_) + 1));
    pipe_pad_ = unit_size_y_ / 10;
    buf_pad_ = unit_size_x_ / 16;
    sfile_.open(filename.c_str(), std::fstream::out | std::ios::trunc);
    sfile_ << "<?xml version=\"1.0\" standalone=\"no\"?> " << std::endl;
    sfile_ << "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">" << std::endl;
    sfile_ << "<svg width=\"" << pixel_x_ << "px\" height=\"" << pixel_y_ << "px\" viewBox=\"0 0 " << width_ << " " << height_ << "\" xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">" << std::endl;
}

Svgss2::~Svgss2()
{
    sfile_ << "</svg>" << std::endl;
    sfile_.close();
}

void Svgss2::drawgrid(void)
{
    char cycleinchar[3]; 
    int cycle = cycle_;
    int line = MAX(h_, inst_ % 2 + inst_);
    int nb_line = line;
    int nb_cycle = cycle;
    int x1, y1, x2, y2;
    for (line = 2; line < nb_line; line = line + 4)
    {
        x1 = left_padding_;
        y1 = line * unit_size_y_ + upper_padding_; 
        x2 = unit_size_x_ * nb_cycle;
        // Putting a rectangle instead of a line causes the same problem while printing from the trac..., so commenting for now
        //sfile_ << "<rect x=\"" << x1 << "\" y=\"" << y1 << "\" width=\"" << x2 << "\" height=\"" << unit_size_y_ * 2 << "\" fill=\"#E0E0E0\" fill-opacity=\"0.5\" stroke-width=\"0\"/>" << std::endl;
    }
    for (; cycle > 0; cycle--)
    {
        x1 = left_padding_ + (cycle - 1) * unit_size_x_;
        y1 = upper_padding_;
        x2 = x1;
        y2 = unit_size_y_ * (nb_line + 1) + upper_padding_; 
        sfile_ << "<line x1=\"" << x1 << "\" y1=\"" << y1 << "\" x2=\"" << x2 << "\" y2=\"" << y2 << "\"";
        sfile_ << " stroke=\"#808080\" stroke-width=\"1\" stroke-dasharray=\""<< unit_size_y_ / 4 << " " << unit_size_y_ / 4 << "\"/>" << std::endl;
    }
    for (cycle = 1; cycle <= nb_cycle; cycle++)
    {
        sprintf(cycleinchar, "%d", cycle);
        drawstring(cycleinchar, cycle, nb_line + 1);
    }
    x1 = left_padding_; 
    y1 = upper_padding_ + nb_line * unit_size_y_; 
    x2 = left_padding_ + nb_cycle * unit_size_x_;
    y2 = y1;
    sfile_ << "<line x1=\"" << x1 << "\" y1=\"" << y1 << "\" x2=\"" << x2 << "\" y2=\"" << y2 << "\"";
    sfile_ << " stroke=\"black\" stroke-width=\"1\" stroke-opacity=\"1.0\"/>" << std::endl;
    sfile_ << "<line x1=\"" << x1 << "\" y1=\"" << y1 << "\" x2=\"" << x2 << "\" y2=\"" << y2 << "\"";
    sfile_ << " stroke=\"black\" stroke-width=\"1\" stroke-opacity=\"1.0\"/>" << std::endl;

}

void Svgss2::drawstring(const std::string & s, int cycle, int line)
{
//    int len = strlen(s.c_str());
    int px = 15 * UNIT_PER_PIXEL;
    int x = ((left_padding_ + cycle * unit_size_x_) - unit_size_x_ / 2);
    int y;
    if (line % 2 == 0) {
       y = upper_padding_ + (line - 1) * unit_size_y_ + unit_size_y_ / 2 + px / 2 - pipe_pad_;
    }
    else {
       y = upper_padding_ + (line - 1) * unit_size_y_ + unit_size_y_ / 2 + px / 2;
    }
    sfile_ << "<text x=\"" << x << "\" y=\"" << y << "\" font-size=\"" << px;
    sfile_ << "px\" font-style=\"italic\" text-anchor=\"middle\" fill=\"#000000\" fill-opacity=\"1\" font-family=\"Monospace\">" << s << "</text>" << std::endl;
}

void Svgss2::drawmstring(const std::string & s, int cycle, int line)
{
//    int len = strlen(s.c_str());
    int px = 15 * UNIT_PER_PIXEL;
    int x = ((left_padding_ + cycle * unit_size_x_) - unit_size_x_ / 2);
    int y = upper_padding_ + line * unit_size_y_ + px / 2 - pipe_pad_;
    sfile_ << "<text x=\"" << x << "\" y=\"" << y << "\" font-size=\"" << px;
    sfile_ << "px\" font-style=\"italic\" text-anchor=\"middle\" fill=\"#000000\" fill-opacity=\"1\" font-family=\"Monospace\">" << s << "</text>" << std::endl;
}

void Svgss2::drawbox(int cycle, int line)
{
    int x = left_padding_ + (cycle - 1) * unit_size_x_;
    int y;
    if (line % 2 == 0) {
       y = upper_padding_ + (line - 1) * unit_size_y_;
    }
    else {
       y = upper_padding_ + pipe_pad_ + (line - 1) * unit_size_y_;
    }
    sfile_ << "<rect x=\"" << x << "\" y=\"" << y << "\" width=\"" << unit_size_x_ << "\" height=\"" << unit_size_y_ - pipe_pad_ << "\" fill=\"none\" stroke=\"black\" stroke-opacity=\"1.0\"/>" << std::endl;
    sfile_ << "<rect x=\"" << x << "\" y=\"" << y << "\" width=\"" << unit_size_x_ << "\" height=\"" << unit_size_y_ - pipe_pad_ << "\" fill=\"none\" stroke=\"black\" stroke-opacity=\"1.0\"/>" << std::endl;
}

void Svgss2::drawmbox(int cycle, int line)
{
    int x = left_padding_ + (cycle - 1) * unit_size_x_;
    int y = upper_padding_ + (line - 1) * unit_size_y_ + (unit_size_y_ + pipe_pad_) / 2; 
    sfile_ << "<rect x=\"" << x << "\" y=\"" << y << "\" width=\"" << unit_size_x_ << "\" height=\"" << unit_size_y_ - pipe_pad_ << "\" fill=\"none\" stroke=\"black\" stroke-opacity=\"1.0\"/>" << std::endl;
    sfile_ << "<rect x=\"" << x << "\" y=\"" << y << "\" width=\"" << unit_size_x_ << "\" height=\"" << unit_size_y_ - pipe_pad_ << "\" fill=\"none\" stroke=\"black\" stroke-opacity=\"1.0\"/>" << std::endl;
}

void Svgss2::drawinst(const std::string & s, int line)
{
    int px = 15 * UNIT_PER_PIXEL;
    int x = left_padding_ / 2;
    int y;
    if (line % 2 == 0) {
       y = (upper_padding_ + (line - 1) * unit_size_y_) + unit_size_y_ / 2 + px / 2 - pipe_pad_;
    }
    else {
       y = (upper_padding_ + (line - 1) * unit_size_y_) + unit_size_y_ / 2 + px / 2 + pipe_pad_;
    }
    sfile_ << "<text x=\"" << x << "\" y=\"" << y << "\" font-size=\"" << px;
    sfile_ << "px\" font-style=\"italic\" text-anchor=\"middle\" fill=\"#000000\" fill-opacity=\"1\" font-family=\"Monospace\">" << s << "</text>" << std::endl;
}

void Svgss2::drawfreeze(int cycle, int line)
{
    int x = left_padding_ + unit_size_x_ * cycle - unit_size_x_ / 2;
    int y = upper_padding_ + unit_size_y_ * line - unit_size_y_ / 2;
    int r = MIN(unit_size_x_, unit_size_y_) / 4;
    sfile_ << "<circle cx=\"" << x << "\" cy=\"" << y << "\" r=\"" << r << "\" stroke=\"black\" fill=\"#FEDCBA\"/>" << std::endl;
}

void Svgss2::drawmfreeze(int cycle, int line)
{
    int x = left_padding_ + unit_size_x_ * cycle - unit_size_x_ / 2;
    int y = upper_padding_ + unit_size_y_ * line;
    int r = MIN(unit_size_x_, unit_size_y_) / 4;
    sfile_ << "<circle cx=\"" << x << "\" cy=\"" << y << "\" r=\"" << r << "\" stroke=\"black\" fill=\"#FEDCBA\"/>" << std::endl;
}

void Svgss2::drawbuffer(void)
{
    int cycle = cycle_ - 2;
    int x1, x2, y1 ,y2;
    
    y1 = 0;
    y2 = upper_padding_;
    for (; cycle >= -1; cycle--)
    {
        x1 = left_padding_ + cycle       * unit_size_x_ + unit_size_x_ / 2 + buf_pad_;
        x2 = left_padding_ + (cycle + 1) * unit_size_x_ + unit_size_x_ / 2 - buf_pad_;
        sfile_ << "<line x1=\"" << x1 << "\" y1=\"" << y1 << "\" x2=\"" << x2 << "\" y2=\"" << y1 << "\"" << " stroke=\"#000000\" stroke-width=\"1\" />" << std::endl;
        sfile_ << "<line x1=\"" << x1 << "\" y1=\"" << y2 << "\" x2=\"" << x2 << "\" y2=\"" << y2 << "\"" << " stroke=\"#000000\" stroke-width=\"1\" />" << std::endl;
        sfile_ << "<line x1=\"" << x1 << "\" y1=\"" << y1 << "\" x2=\"" << x1 << "\" y2=\"" << y2 << "\"" << " stroke=\"#000000\" stroke-width=\"1\" />" << std::endl;
        sfile_ << "<line x1=\"" << x2 << "\" y1=\"" << y1 << "\" x2=\"" << x2 << "\" y2=\"" << y2 << "\"" << " stroke=\"#000000\" stroke-width=\"1\" />" << std::endl;
    }
}

void Svgss2::writeinbuf(const std::string & s, int cycle, int num)
{
    int px = 12 * UNIT_PER_PIXEL;
    int x = 5 * UNIT_PER_PIXEL + ((left_padding_ + cycle * unit_size_x_) + unit_size_x_ / 2);
    int y = (num + 1) * px;
    std::string inst_name = s;
    if (inst_name == "nop*") {
       inst_name = "";
    }
    sfile_ << "<text font-size=\"" << px << "px\" font-family=\"liberation serif\" font-style=\"italic\" x=\"" << x << "\" y=\"" << y << "\">" << inst_name << "</text>" << std::endl;
}

void Svgss2::drawbypass(int cycle, int inst1, int inst2, int color, int m)
{
    int x = left_padding_ + unit_size_x_ * (cycle + 1);
    int y = upper_padding_ + unit_size_y_ * inst1 - unit_size_y_ / 2;
    int y2 = upper_padding_ + unit_size_y_ * inst2 - unit_size_y_ / 2;
    if (m == 1 || m == 3)
        y += (unit_size_y_ / 2);
    if (m == 2 || m == 3)
        y2 += (unit_size_y_ / 2);
    int r = MIN(unit_size_x_, unit_size_y_) / 8;
    std::string color_c = std::string("red");
    if (color == 1)
        color_c = std::string("orange");
    if (color == 2)
        color_c = std::string("blue");

    sfile_ << "<circle cx=\"" << x + 3 * r - 2 << "\" cy=\"" << y << "\" r=\"" << r << "\" fill=\"" << color_c << "\"/>" << std::endl;
    sfile_ << "<circle cx=\"" << x + r << "\" cy=\"" << y2 << "\" r=\"" << r << "\" fill=\"" << color_c << "\"/>" << std::endl;
    sfile_ << "<line x1=\"" << x + 2 * r - 1 << "\" y1=\"" << y << "\" x2=\"" << x + 2 * r - 1 << "\" y2=\"" << y2 << "\"";
    sfile_ << " stroke=\"" << color_c << "\" stroke-width=\"" << 1.5 * UNIT_PER_PIXEL <<"\" stroke-opacity=\"1.0\"/>" << std::endl;
    sfile_ << "<line x1=\"" << x + 2 * r - 1 << "\" y1=\"" << y << "\" x2=\"" << x + 2 * r - 1 << "\" y2=\"" << y2 << "\"";
    sfile_ << " stroke=\"" << color_c << "\" stroke-width=\"" << 1.5 * UNIT_PER_PIXEL << "\" stroke-opacity=\"1.0\"/>" << std::endl;
}

void Svgss2::output(EmulProg * emul)
{
    string * insts = emul->get_insts();
    string ** data = emul->get_data();
    int nb_cycle = cycle_;
    int nb_insts = inst_;
    int bypass_color[nb_cycle];
    int nb_bypass = emul->get_nb_bypass();
    bypass_t * bypass = emul->get_bypass();
    drawgrid();
    for (int i = 0; i < nb_insts; i++)
        drawinst(insts[i], i + 1);
    for (int i = 0; i < MAX(h_, inst_ % 2 + inst_); i++)
    {
        for (int j = 0; j < nb_cycle; j++)
        {
            if (data[i][j] != "")
            {
                if (data[i][j][0] == 'm')
                {
                    if (data[i][j] == "mC")
                    {
                        drawmbox(j + 1, i + 1);
                        drawmfreeze(j + 1, i + 1);
                    }
                    else if (data[i][j] == "mI")
                    {
                        drawmbox(j + 1, i + 1);
                        drawmstring("I", j + 1, i + 1);
                    }
                    else
                    {
                        drawmbox(j + 1, i + 1);
                        drawmstring("D", j + 1, i + 1);
                    }
                }
                else
                {
                    if (data[i][j] == "C")
                    {
                        drawbox(j + 1, i + 1);
                        drawfreeze(j + 1, i + 1);
                    }
                    else
                    {
                        drawbox(j + 1, i + 1);
                        drawstring(data[i][j].substr(0,1), j + 1, i + 1);
                    }
                }
            }
        }
    }

    drawbuffer();
    for (int i = 0; i < nb_cycle; i++)
        bypass_color[i] = 0;
    for (int i = nb_bypass - 1; i > -1; i--)
        drawbypass(bypass[i].cycle - 1, bypass[i].inst1 + 1, bypass[i].inst2 + 1, bypass_color[bypass[i].cycle]++ % 3, bypass[i].m);
    buffer_t * buf = emul->get_buf();
    for (int i = 0; i != emul->get_nb_buf() - 1; i++)
    {
        writeinbuf(buf[i].inst1, i, 0);
        writeinbuf(buf[i].inst2, i, 1);
        writeinbuf(buf[i].inst3, i, 2);
        writeinbuf(buf[i].inst4, i, 3);
    }

}

