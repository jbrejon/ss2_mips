
#include <iostream>
#include <iomanip>

#include "EmulProg.h"

#define min(x, y) ((x) < (y) ? (x) : (y))

EmulProg::EmulProg(int verbose)
{
    verbose_ = verbose;
    instructions_ = new string[MAX_INST];
    data_ = new string*[MAX_INST];
    for (int i = 0; i < MAX_INST; i++)
        data_[i] = new string[MAX_CYCLE];
    bypass_ = new bypass_t[MAX_BYPASS];
    buffer_ = new buffer_t[MAX_CYCLE];
    last_pos_i_ = 0;
    nb_inst_ = 0;
    nb_cycle_ = 0;
    nb_bypass_ = 0;
    nb_buf_ = 0;
    h_ = 0;
}

int  EmulProg::add_inst(string name)
{
    if (nb_inst_ == MAX_INST)
        return -1;
    instructions_[nb_inst_] = name;
    nb_inst_++;
    return 0;
}

void EmulProg::add_data(string dat, int inst_number)
{
    if (dat == "mI")
    {
        while(data_[last_pos_i_][nb_cycle_] != "")
            last_pos_i_ += 2;
        data_[last_pos_i_][nb_cycle_] = dat;
        return;
    }
    data_[inst_number][nb_cycle_] = dat;
}

int EmulProg::add_bypass(int cycle, int inst1, int inst2, int op, int m)
{
    for (int i = 0; i < nb_bypass_; i++)
    {
        if (bypass_[i].inst1 == inst1 &&
                bypass_[i].inst2 == inst2 &&
                bypass_[i].op    == op &&
                bypass_[i].m     == m)
        {
            bypass_[i].cycle = cycle;
            bypass_[i].inst1 = inst1;
            bypass_[i].inst2 = inst2;
            bypass_[i].op    = op;
            bypass_[i].m     = m;
            return 1;
        }
    }
    bypass_[nb_bypass_].cycle = cycle;
    bypass_[nb_bypass_].inst1 = inst1;
    bypass_[nb_bypass_].inst2 = inst2;
    bypass_[nb_bypass_].op    = op;
    bypass_[nb_bypass_].m     = m;

    nb_bypass_++;
    return 0;
}

void EmulProg::inc_h(void)
{
    h_ += 2;
}

int EmulProg::get_h(void)
{
    return min(h_, last_pos_i_);
}

void EmulProg::inc_cycle(void)
{
    nb_cycle_++;
}

int EmulProg::get_inst(void)
{
    return nb_inst_;
}

int EmulProg::get_cycle(void)
{
    return nb_cycle_;
}

string ** EmulProg::get_data(void)
{
    return data_;
}

int EmulProg::get_nb_bypass(void)
{
    return nb_bypass_;
}

int EmulProg::get_nb_buf(void)
{
    return nb_buf_;
}

bypass_t * EmulProg::get_bypass(void)
{
    return bypass_;
}

void EmulProg::new_buf(string i1, string i2, string i3, string i4)
{
    buffer_[nb_buf_].inst1 = i1;
    buffer_[nb_buf_].inst2 = i2;
    buffer_[nb_buf_].inst3 = i3;
    buffer_[nb_buf_].inst4 = i4;
    nb_buf_++;
}

buffer_t * EmulProg::get_buf(void)
{
    return buffer_;
}

string * EmulProg::get_insts(void)
{
    return instructions_;
}

void EmulProg::display(void)
{
    if (verbose_ == 0)
    {
        return;
    }
    std::cout << "### Emul ###" << std::endl;
    std::cout << "instructions : " << nb_inst_ << " - cycles : " << nb_cycle_ << std::endl;
    std::cout << "  \t"; 
    for (int cycle = 1; cycle < nb_cycle_ + 1; cycle++)
    {
        std::cout << setw(3) << cycle;
    }
    std::cout << std::endl;
    for (int i = 0; i < nb_inst_; i++)
    {
        std::cout << instructions_[i] << "\t";
        for (int j = 0; j < nb_cycle_; j++)
        {
            if (data_[i][j] == "")
            {
                std::cout << "   ";
            }
            else if (data_[i][j] == "C")
            {
                std::cout << " FZ";
            }
            else
            {
                std::cout << " " << data_[i][j];
            }
        }
        std::cout << std::endl;
    }
    std::cout << "### Bypasses ###" << std::endl;
    for (int i = 0; i < nb_bypass_; i++)
    {
        std::cout << "cycle : " << setw(2) << bypass_[i].cycle << " | inst1 : " << setw(2) << bypass_[i].inst1 << " | inst2 : " << setw(2) << bypass_[i].inst2 << " | op : " << bypass_[i].op << std::endl;
    }
}



