loop:
    lbu   $10, 0($8)    ; lire un pixel
    addiu $8 , $8 , 1
    addu  $10, $10, $9
    lbu   $11, 0($10)   ; lire f(pixel)

    bne   $8 , $12, loop
    sb    $11, -1($8)

