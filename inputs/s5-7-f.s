
loop:
    Lw    $7 , 0($5)    ; lire l'élément i
    Addiu $5 , $5 , 8
    Lw    $17, -4($5)   ; lire l'élément i+1
    Bgez  $7 , _endif1
    Nop
    Sub   $7 , $0 , $7  ; l'opposé de i
    Sw    $7 , -8($5)

_endif1:
    Bgez  $17, _endif2
    Nop
    Sub   $17, $0 , $17 ; l'opposé de i+1
    Sw    $17, -4($5)

_endif2:
    Bne   $5 , $6 , loop
    Nop

