
loop:
    Lbu   r10, 0(r8)   ; lire un pixel i
    Addiu r8 , r8 , 2
    Lbu   r20, -1(r8)  ; lire un pixel i+1
    Addu  r10, r10, r9
    Addu  r20, r20, r9
    Lbu   r11, 0(r10)  ; lire f(pixel i  )
    Lbu   r21, 0(r20)  ; lire f(pixel i+1)
    Sb    r11, -2(r8)  ; sauvegarde pixel i

    Bne   r8 , r12, loop
    Sb    r21, -1(r8)  ; sauvegarde pixel i+1

