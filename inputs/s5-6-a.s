loop:
    lbu   $10, 0($5)
    lbu   $11, 0($6)
    sb    $11, 0($5)
    sb    $10, 0($6)

    addiu $6 , $6 , -1
    addiu $5 , $5 ,  1
    bne   $5 , $7 , loop
    nop
