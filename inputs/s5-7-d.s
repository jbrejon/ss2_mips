
loop:
    Lw    r7 , 0(r5)    ; lire un élément
    Addiu r5 , r5 , 4
    Bgez  r7 , _endif
    Nop
    Sub   r7 , r0 , r7  ; l'opposé
    Sw    r7 , -4(r5)

_endif:
    Bne   r5 , r6 , loop
    Nop


