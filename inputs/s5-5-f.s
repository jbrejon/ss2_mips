
loop:
    Sb    r13, -3(r8)    ; sauvegarde de pixel i-3
    Addiu r8 , r8 , 1
    Lbu   r13, 0(r11)    ; lire f(pixel i-2)
    Addu  r11, r10, r9   ; calcul adresse de f(i-1)

    Bne   r8 , r12, loop
    Lbu   r10, -1(r8)    ; lire le pixel i


