loop:
    lw    $7 , 0($5)    ; lire un élément
    bgez  $7 , _endif
    nop
    sub   $7 , $0 , $7  ; calculer l'opposé
    sw    $7 , 0($5)

_endif:
    addiu $5 , $5 , 4
    bne   $5 , $6 , loop
    nop
