loop:
    lbu   $10, 0($8)    ; lire un pixel
    addu  $10, $10, $9
    lbu   $11, 0($10)   ; lire f(pixel)
    sb    $11, 0($8)

    addiu $8 , $8 , 1
    bne   $8 , $12, loop
    nop

