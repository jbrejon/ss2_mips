_strupper_loop:
    lb    $8 , 0($4)        ; lire src[i]
    slt   $9 , $8 , $11     ; src[i] < 'a'
    slt   $10, $12, $8      ; 'z' < src[i]
    or    $10, $10, $9      ; et des 2 conditions
    bne   $10, $0 , _strupper_endif ; si 1 des 2 cond vraie
    nop
    addi  $8 , $8 , 'A'-'a' ; transformer en majuscule
    sb    $8, 0($4)         ; écrire src[i]

_strupper_endif:
    addiu $4 , $4 , 1       ; caractère suivant
    bne   $8 , $0 , _strupper_loop
    nop

