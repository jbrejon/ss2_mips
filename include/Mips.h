
#ifndef _MIPS_H
#define _MIPS_H
#include <string>
#include <iostream>
#include <sstream>

#include <assert.h>

#include "Instruction.h"
#include "InstData.h"
#include "Enum_type.h"
#include "Stages.h"
#include "Program.h"
#include "EmulProg.h"

class Program;
class InstData;
class Stages;
class EmulProg;

using namespace std;

enum final_branch_state_e_ {
   NONE,
   WAITING_FINAL_BRANCH,
   WAITING_FINAL_INST
};

class Mips
{
    public :
        Mips(Program * prog, int * branch, int alignement, int verbose);
        ~Mips();
        int do_cycle(void);
        void set_mips_stages(void);
        void display_InstData(void);
        void set_pc(int pc);
        void set_nextpc(int nextpc);
        int get_pc(void);
        int get_cycle(void);
        int get_branch(void);
        int get_nb_stages(void);
        int * get_prod_reg(void);
        void fill_cons_reg(int cons_reg[][2], int inst);
        void fill_prod_reg(int prod_reg_p[][2], int inst);
        int fill_buffer(void);
        int get_stage(char c);
        void emul_add_inst(int inst);
        int is_nop(int inst);
        int set_inst_stage(int inst, int stage);
        int check_end(void);
        int get_stage_inst(int stage);
        int get_stage_old_inst(int stage);
        int get_emul_inst(int stage_nb);
        int get_old_emul_inst(int stage_nb);
        void set_emul_inst(int stage_nb, int einst);
        void set_next_emul_inst(int stage_nb, int einst);
        Program * get_prog(void);
        EmulProg * get_emul_prog(void);
        int check_code(void);
        int check_arch_error(void);
        int get_buf_i(int i);
        int pop_buf(void);
        void clr3_buf(void);
        void write_buf(void);
        void display_stages(int cycle);
        int get_shift_state(int i);
        void set_final(int br, int inst);

    private :
        Program * prog_;
        Stages ** stages_;
        InstData ** Dinst_;
        EmulProg * emul_;
        int * branch_;
        int branchi_;
        int alignement_;
        int pc_;
        int cycle_;
        int nextpc_;
        int nb_insts_;
        int final_branch_;
        int final_inst_;
        int final_branch_state_;
        int final_fill_buf_;
        // first half of prod reg contains the register number
        // second half of prod reg contains the number of stage at which the instruction will produce the register number
        int nb_free_slot_;
        int buffer_[4];
        int shift_states_[8];
        int verbose_;
};
#endif

