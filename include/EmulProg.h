
#ifndef _EMUL_PROG_H
#define _EMUL_PROG_H
#define MAX_INST 30
#define MAX_CYCLE 30
#define MAX_BYPASS 30
#include <string>
#include <map>

using namespace std;

typedef struct bypass_s
{
    int cycle;
    int inst1;
    int inst2;
    int op;
    int m;
} bypass_t;

typedef struct buffer_s
{
    string inst1;
    string inst2;
    string inst3;
    string inst4;
} buffer_t;

class EmulProg
{
    public:
        EmulProg(int verbose);
        ~EmulProg();
        int add_inst(string name);
        void add_data(string dat, int inst_number);
        int add_bypass(int cycle, int inst1, int inst2, int op, int m);
        void inc_cycle(void);
        void display(void);
        int get_cycle(void);
        int get_inst(void);
        void new_buf(string i0, string i1, string i2, string i3);
        buffer_t * get_buf(void);
        int get_nb_buf(void);
        string ** get_data(void);
        string * get_insts(void);
        int get_nb_bypass(void);
        bypass_t * get_bypass(void);
        void inc_h();
        int get_h();

    private:
        int last_pos_i_;
        int nb_inst_;
        int h_;
        int nb_cycle_;
        int nb_bypass_;
        int nb_buf_;
        string * instructions_;
        string ** data_;
        bypass_t * bypass_; //cycle, inst1, inst2
        buffer_t * buffer_;
        int verbose_;
};
#endif

