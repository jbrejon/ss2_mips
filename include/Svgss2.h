
#ifndef SVGSS2_H
#define SVGSS2_H

#include <string>
#include <iostream>
#include <fstream>
#include "EmulProg.h"

class Svgss2
{
    private :
        std::fstream sfile_;
        int unit_size_x_;
        int unit_size_y_;
        int pixel_x_;
        int pixel_y_;
        int left_padding_;
        int upper_padding_;
        int width_;
        int height_;
        int pipe_pad_;
        int buf_pad_;
        int cycle_;
        int inst_;
        int h_;

    public :
        Svgss2(const std::string & filename, int cycle, int inst, int h);
        ~Svgss2();
        void drawgrid(void);
        void drawstring(const std::string & s, int cycle, int line);
        void drawmstring(const std::string & s, int cycle, int line);
        void drawbox(int cycle, int line);
        void drawmbox(int cycle, int line);
        void drawfreeze(int cycle, int line);
        void drawmfreeze(int cycle, int line);
        void drawbuffer(void);
        void writeinbuf(const std::string & s, int cycle, int num);
        void drawinst(const std::string & s, int line);
        void drawbypass(int cycle, int inst1, int inst2, int color, int m);
        void output(EmulProg * emul);
};

#endif

