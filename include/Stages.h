
#ifndef _STAGES_H
#define _STAGES_H
#include <string>
#include <map>

#include "Mips.h"
using namespace std;

class Mips;

class Stages
{
    public:
        Stages(char * name, int bypass, int calc_addr, int stage_number);
        ~Stages();
        int verif_bypass(int fromshift);
        int exec(int inst);
        int shift(void);
        void set_mips(Mips * mips);
        int set_inst(int inst);
        int get_inst(void);
        int get_old_inst(void);
        int get_emul_inst(void);
        int get_old_emul_inst(void);
        void set_emul_inst(int einst);
        void set_next_emul_inst(int einst);

    private:
        string name_;
        int instruction_; //instruction's id currently in the stage
        int last_cycle_was_a_reset;
        int old_inst_;
        int bypass_;
        Mips * mips_;
        int inst_ready_;
        int calc_addr_;
        int stage_number_;
        int emul_inst_;
        int old_emul_inst_;
        int next_emul_inst_;
        int branch_handling(int inst, int stagetotest);
};
#endif

