
#ifndef _Program_H
#define _Program_H
#include <string>
#include <map>

using namespace std;
class Instruction;
class Program
{
    public:
        Program ();
        Program (string const file);
        ~Program();

        void add_line(string label);
        void add_line(Instruction * inst);
        void inval_label(void);
        void display(void);
        int get_nb_insts(void);
        int get_label(string s);
        int check_labels(void);
        Instruction ** get_instructions(void);

    private:
        int latest_line_is_label_;
        string latest_label_;
        Instruction * insts_[30];
        map<string, int> labels_;
        map<string, int> inval_labels_;

        int length_;
        int errors_;
};
#endif

